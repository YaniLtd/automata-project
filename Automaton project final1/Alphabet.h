#pragma once
#include <iostream>
#include "String.h"
template<typename T>
class Alphabet
{
public:
	Alphabet();
	Alphabet(T *, unsigned);
	Alphabet(const Alphabet<T> &);
	Alphabet<T> & operator=(const Alphabet<T> &);
	~Alphabet();
	unsigned getLettersCnt() const;
	const T* getLetters() const;
	void setLettersCnt(unsigned);
	T& operator [](unsigned idx) {
		return alphabet[idx];
	}
	T operator [](unsigned idx) const {
		return alphabet[idx];
	}
	bool containsLetter(T, unsigned);
	bool operator==(const Alphabet<T> &);



private:
	T * alphabet;
	unsigned lettersCount;
	bool  charCase = (toString(typeid(T).name()) == "char");
};

template<typename T>
Alphabet<T>::Alphabet()
{
	lettersCount = 0;
	alphabet = nullptr;
}

template<typename T>
Alphabet<T>::Alphabet(T * alphabet, unsigned sz)
	: lettersCount(sz)
{
	if (charCase)
	{
		alphabet = new T[lettersCount + 1];
		alphabet[lettersCount] = '\0';
	}
	else
	{
		alphabet = new T[lettersCount];
	}
	for (size_t i = 0; i < lettersCount; i++)
	{
		alphabet[i] = obj.alphabet[i];
	}
}

template<typename T>
Alphabet<T>::Alphabet(const Alphabet<T> & obj)
	:lettersCount(obj.lettersCount)
{
	if (charCase)
	{
		alphabet = new T[lettersCount + 1];
		alphabet[lettersCount] = '\0';
	}
	else
	{
		alphabet = new T[lettersCount];
	}
	for (size_t i = 0; i < lettersCount; i++)
	{
		alphabet[i] = obj.alphabet[i];
	}
}

template<typename T>
Alphabet<T>::~Alphabet()
{
	if (alphabet != nullptr)
	{
		delete[] alphabet;
	}
}

template<typename T>
Alphabet<T> & Alphabet<T>::operator=(const Alphabet<T> & rhs)
{
	if (this != &rhs)
	{
		if (alphabet != nullptr)
		{
			delete[] alphabet;
		}

		lettersCount = rhs.lettersCount;

		if (charCase)
		{
			alphabet = new T[lettersCount + 1];
			alphabet[lettersCount] = '\0';
		}
		else
		{
			alphabet = new T[lettersCount];
		}
		for (size_t i = 0; i < lettersCount; i++)
		{
			alphabet[i] = rhs.alphabet[i];
		}
	}

	return *this;
}

template<typename T>
unsigned Alphabet<T>::getLettersCnt() const
{
	return lettersCount;
}

template<typename T>
const T * Alphabet<T>::getLetters() const
{
	return alphabet;
}

template<typename T>
void Alphabet<T>::setLettersCnt(unsigned cnt)
{
	lettersCount = cnt;
	if (charCase)
	{
		alphabet = new T[lettersCount + 1];
		alphabet[lettersCount] = '\0';
		return;
	}
	alphabet = new T[lettersCount];
}



template<typename T>
bool Alphabet<T>::containsLetter(T l, unsigned bound)
{
	for (size_t i = 0; i < bound; i++)
	{
		if (alphabet[i] == l)
		{
			return true;
		}
	}
	return false;
}

template<typename T>
bool Alphabet<T>::operator==(const Alphabet<T>& rhs)
{
	if (lettersCount != rhs.lettersCount)
	{
		return false;
	}
	for (size_t i = 0; i < lettersCount; i++)
	{
		if (rhs.alphabet[i] != alphabet[i])
		{
			return false;
		}
	}
	return true;
}


