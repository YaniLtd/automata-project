#pragma once
#include <iostream>
#include "DFA.h"

template<typename T> 
std::ostream& operator<<(std::ostream& out, AbstractDFA<T> * automaton) {
	unsigned statesCnt = automaton->getStatesCnt();
	out << "\n";
	out << "States: \n   ";
	for (size_t i = 0; i < statesCnt; i++)
	{
		out << automaton->getStates()[i];
	}
	out << "\n";

	unsigned alphSZ = automaton->getAlphabetSymbolsCnt();
	out << "\n";
	out << "Alphabet: \n   ";
	for (unsigned i = 0; i < alphSZ; i++)
	{
		out << automaton->getAlphabet()[i] << " ";
	}

	out << "\n";
	automaton->printTable(out, automaton->getStates(), automaton->getAlphabet());
	out << "\n";

	return out;
}