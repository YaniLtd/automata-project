#pragma once
#include <iostream>
#include <fstream>
#include "DFA.h"


//2. ���� �������� ���� �����������: 2
template<typename T>
void readStatesCnt(std::ifstream& fin, AbstractDFA<T> * p)
{
	unsigned cnt;
	fin >> cnt;
	if (0 == cnt || cnt > 50)
	{
		std::cerr << "Invalid number of states!!!\nNumber of states must in range [1-50]!\n";
		throw AutomatonException("Invalid number of states!!!");
	}
	p->setStatesCnt(cnt);
}


/*
3. ���� �������� ��������� �����[1]: q0
4. ���� �������� ��������� �����[2] : q1
 ? ������� e ����� �� �����������, ������� �� ��� 2, ������� �� ���� ��������� �
���������� �� �� ������� ������������.? /
*/
template<typename T>
void readStates(std::ifstream& fin, AbstractDFA<T> * p)
{
	unsigned sz = p->getStatesCnt();
	String stateName;
	for (size_t i = 0; i < sz; i++)
	{
		fin >> stateName;
		if (!p->containsState(stateName, i))
		{
			p->setStateNameAt(stateName, i);
		}
		else
		{
			std::cerr << "State name already exists!!!\n";
			throw AutomatonException("State name already exists!!!\n");
		}
	}

}

//5. ���� �������� ���� ������� � �������� �� ��������: 2
template<typename T>
void readAlphabetSymbolsCnt(std::ifstream& fin, AbstractDFA<T> * p)
{
	unsigned cnt;
	fin >> cnt;
	if (0 == cnt || cnt > 10)
	{
		std::cerr << "Number of symbols entered was not in range [1-10]!!!!!\n";
		throw AutomatonException("Number of symbols entered was not in range [1-10]!!!!!\n");
	}
	p->setAlphabetSymbolsCnt(cnt);

}


/*
6. ���� �������� ������ ����� [1]: 0
7. ���� �������� ������ ����� [2]: 1
? ������� � ����� �� ��������� � ��������, ������� �� ��� 5, ������� �� ����
������� �� �������� � ���������� �� �� ������� ������������. ?/
*/
template<typename T>
void readAlphabet(std::ifstream& fin, AbstractDFA<T> * p)
{
	T letter;
	unsigned sz = p->getAlphabetSymbolsCnt();
	for (size_t i = 0; i < sz; i++)
	{
		fin >> letter;
		if (p->getAlphabet().containsLetter(letter, i))
		{
			std::cerr << "Letter already exists!!!\n";
			throw AutomatonException("Letter already exists!!!\n");
		}
		else
		{
			p->setLetterAt(letter, i);
		}
	}
}


/*
? �� ���� �� ���� ��������� N � ���� �������� �� �������� , �� �� ������� ��
����������� �� �� ������ Transition table � N ?M ��������. � ����� ������ �����
2 ��������� � 2 �������� �� ��������, ������������ 4 �������� � ��������� �
���������, ������ �� ����� ��������� � ������ �� ������ ��� ��� ��������� ���� ?/

[8-11]
*/
template<typename T>
void readTransTable(std::ifstream& fin, AbstractDFA<T> * p)
{
	unsigned rows = p->getStatesCnt();
	unsigned cols = p->getAlphabetSymbolsCnt();
	p->createTable(rows, cols);
	State input;
	for (size_t i = 0; i < rows; i++)
	{
		for (size_t j = 0; j < cols; j++)
		{
			fin >> input;
			if (p->containsState(input))
			{
				p->setTableElement(input, i, j);
			}
			else
			{
				std::cerr << "State does not exist!!!\n";
				throw AutomatonException("State does not exist!!!\n");
			}
		}
	}
	//p->printTable(std::cout, p->getStates(), p->getAlphabet());

}

//12. ���� �������� ������� ���������: q0
template<typename T>
void readInitialState(std::ifstream& fin, AbstractDFA<T> * p)
{
	if (p->hasInitial())
	{
		std::cerr << "The automaton already has an initial state!!!\n";
		throw AutomatonException("The automaton already has an initial state!!!\n");
	}
	char stateName[10];
	int idx;
	fin >> stateName;
	idx = p->containsStateAt(stateName);
	if (idx == -1)
	{
		std::cerr << "Wrong state name!!!\n";
		throw AutomatonException("Wrong state name!!!\n");
	}
	else
	{
		p->makeInitialState(idx);
	}

}

//13. ���� �������� ���� ������ ���������: 1
template<typename T>
void readFinalStates(std::ifstream& fin, AbstractDFA<T> * p)
{
	unsigned fStatesCnt;
	unsigned statesCnt = p->getStatesCnt();
	fin >> fStatesCnt;
	if (fStatesCnt > statesCnt || fStatesCnt == 0)
	{
		std::cerr << "The number of final states must be between [" << 1 << "-" << statesCnt << "]" << "!!!\n";
		throw AutomatonStateException("The number of final is not valid!!!\n");
	}

	char stateName[10];
	int idx;
	for (size_t i = 0; i < fStatesCnt; i++)
	{
		fin >> stateName;
		idx = p->containsStateAt(stateName);
		if (idx == -1)
		{
			std::cerr << "Wrong state name!!!\n";
			throw AutomatonStateException("Wrong state name!!!\n");
		}
		else if (p->getStates()[idx].isFinalState())
		{
			std::cerr << "This state is already final!!!\n";
			throw AutomatonStateException("This state is already final!!!\n");
		}
		else
		{
			p->makeFinalState(idx);
		}
	}
}




template<typename T>
std::ifstream& operator>>(std::ifstream & fin, AbstractDFA<T> * p)
{
	readStatesCnt(fin, p);
	readStates(fin, p);
	readAlphabetSymbolsCnt(fin, p);
	readAlphabet(fin, p);
	readTransTable(fin, p);
	readInitialState(fin, p);
	readFinalStates(fin, p);
	p->setInitialAndFinalStates();

	p->isValid();
	return fin;
}
void fileInput(AbstractDFA<int> *(&intAutomata)[5], AbstractDFA<char> * (&charAutomata)[5], unsigned & idxI, unsigned & idxCh);

