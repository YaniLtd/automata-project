#pragma once
#include "State.h"
#include "String.h"
#include "Alphabet.h"
#include <iomanip>

template<typename T>
class TransTable
{
public:
	TransTable();
	TransTable(unsigned, unsigned, State**);
	TransTable(const TransTable&);
	~TransTable();
	TransTable<T> operator=(const TransTable&);
	//void setTable(unsigned, unsigned, AbstractDFA<T> *);


	void createTable(unsigned, unsigned);
	void setElement(const State &, unsigned, unsigned);
	void print(std::ostream &, State*, const Alphabet<T> &);
	void makeFinalState(unsigned, unsigned);
	void reverseFinalStates();
	void makeInitialState(unsigned, unsigned);

	String getStateName(unsigned, unsigned) const;
	State getState(unsigned, unsigned) const;
	State getInitialState() const;
	unsigned getColsCnt() const;
	unsigned getRowsCnt() const;
	unsigned getStatesCnt() const;

	
	friend std::ostream & operator<<(std::ostream & out, const TransTable<T>& t) {
		for (size_t i = 0; i < t.rows; i++)
		{
			for (size_t j = 0; j < t.cols; j++)
			{
				out << t.getState(i, j) << " ";
			}
			out << "\n";
		}
		return out;
	}
private:
	State** table;
	unsigned cols;
	unsigned rows;
};

template<typename T>
TransTable<T>::TransTable()
	: table(nullptr), cols(0), rows(0) {}

template<typename T>
TransTable<T>::TransTable(unsigned r, unsigned c, State** t)
	: rows(r), cols(c)
{
	createTable(rows, cols);
	for (size_t i = 0; i < rows; i++)
	{
		for (size_t i = 0; i < cols; i++)
		{
			table[i][j] = t[i][j];
		}
	}
}

template<typename T>
TransTable<T>::TransTable(const TransTable& rhs) : rows(rhs.rows), cols(rhs.cols)
{
	createTable(rows, cols);
	for (size_t i = 0; i < rows; i++)
	{
		for (size_t j = 0; j < cols; j++)
		{
			table[i][j] = rhs.table[i][j];
		}
	}
}

template<typename T>
TransTable<T>::~TransTable()
{
	if (table != nullptr)
	{
		for (size_t i = 0; i < rows; i++)
		{
			if (table[i] != nullptr)
			{
				delete[] table[i];
			}
		}
		delete[] table;
	}
}


template<typename T>
TransTable<T> TransTable<T>::operator=(const TransTable& rhs)
{
	if (this != &rhs)
	{
		if (table != nullptr)
		{
			for (size_t i = 0; i < rhs.rows; i++)
			{
				delete[] table[i];
			}
			delete[] table;
		}
		rows = rhs.rows;
		cols = rhs.cols;
		createTable(rows, cols);
		for (size_t i = 0; i < rows; i++)
		{
			for (size_t j = 0; j < cols; j++)
			{
				table[i][j] = rhs.table[i][j];
			}
		}
	}
	return *this;

}

template<typename T>
void TransTable<T>::print(std::ostream & out, State* states, const Alphabet<T> & alphabet)
{
	out << "\n" << "Trans table:\n";
	unsigned alphabetSZ = alphabet.getLettersCnt();
	out << setw(10) << " ";
	for (size_t i = 0; i < alphabetSZ; i++)
	{
		out << setw(10) << alphabet[i];
	}
	out << "\n";
	for (size_t i = 0; i < rows; i++)
	{
		out << std::right << setw(10) << states[i].getName();
		for (size_t j = 0; j < cols; j++)
		{
			out << setw(10) << table[i][j].getName();
		}
		out << "\n";
	}
}

template<typename T>
void TransTable<T>::makeFinalState(unsigned i, unsigned j)
{
	table[i][j].makeFinal();
}

template<typename T>
void TransTable<T>::reverseFinalStates()
{
	for (size_t i = 0; i < rows; i++)
	{
		for (size_t j = 0; j < cols; j++)
		{
			if (table[i][j].isFinalState())
			{
				table[i][j].unfinalise();
			}
			else
			{
				table[i][j].makeFinal();
			}
		}
	}
}

template<typename T>
void TransTable<T>::makeInitialState(unsigned i, unsigned j)
{
	table[i][j].makeInitial();
}

template<typename T>
String TransTable<T>::getStateName(unsigned i, unsigned j) const
{
	return table[i][j].getName();
}

template<typename T>
State TransTable<T>::getState(unsigned i, unsigned j) const
{
	return table[i][j];
}

template<typename T>
State TransTable<T>::getInitialState() const
{
	for (size_t i = 0; i < rows; i++)
	{
		if (table[i][0].isInitialState())
		{
			return table[i][0];
		}
	}
	return State();
}

template<typename T>
unsigned TransTable<T>::getColsCnt() const
{
	return cols;
}

template<typename T>
unsigned TransTable<T>::getRowsCnt() const
{
	return rows;
}

template<typename T>
unsigned TransTable<T>::getStatesCnt() const
{
	return cols*rows;
}





template<typename T>
void TransTable<T>::createTable(unsigned r, unsigned c)
{
	table = new State*[r];
	for (size_t i = 0; i < r; i++)
	{
		table[i] = new State[c];
	}
	rows = r;
	cols = c;
}


template<typename T>
void TransTable<T>::setElement(const State & state, unsigned i, unsigned j)
{
	table[i][j] = state;
}


//template<typename T>
//std::ostream & operator<<(std::ostream & out, const TransTable<T>& t)
//{
//	for (size_t i = 0; i < t.rows; i++)
//	{
//		for (size_t j = 0; j < t.cols; j++)
//		{
//			out << t.getState(i, j) << " ";
//		}
//		out << "\n";
//	}
//	return out;
//}