#include "String.h"
#include <iostream>
#include <sstream>
#pragma warning(disable : 4996)



String::String() : str(nullptr), sz(0) {}

String::String(char * charStr)
{
	sz = strlen(charStr) + 1;
	str = new char[sz];
	strcpy_s(str, sz, charStr);
	sz--;
}
String::String(const String& rhs)
{
	char * charStr = rhs.str;

	sz = strlen(charStr) + 1;
	str = new char[sz];
	strcpy_s(str, sz, charStr);
	sz--;
}
String::~String()
{
	if (str != nullptr)
	{
		delete[] str;
	}
}

String & String::operator=(char * charStr)
{
	if (str != nullptr)
	{
		delete[] str;
	}
	sz = strlen(charStr) + 1;
	str = new char[sz];
	strcpy_s(str, sz, charStr);
	sz--;
	return *this;
}
String String::operator+(char * charStr)
{
	unsigned resSZ = strlen(charStr) + sz + 1;
	char * res = new char[resSZ];
	strcpy_s(res, sz + 1, str);
	strcat(res, charStr);
	res[resSZ - 1] = '\0';

	String tmp(res);
	delete[] res;
	return tmp;
}
String& String::operator+=(char * charStr)
{
	(*this) = (*this) + charStr;
	return *this;
}

bool String::operator==(char * charStr)
{
	return strcmp(str, charStr) == 0 ? true : false;
}


bool String::operator==(const String & rhs)
{
	return strcmp(str, rhs.str) == 0 ? true : false;
}

String & String::operator=(const String& rhs)
{
	if (this != &rhs)
	{
		char * charStr = rhs.str;
		*this = charStr;
		/*if (str != nullptr)
		{
		delete[] str;
		}
		sz = strlen(charStr);
		str = new char[sz + 1];
		strcpy_s(str, sz + 1, charStr);*/
	}

	return *this;
}
String String::operator+(const String& rhs)
{
	char * charStr = rhs.str;

	unsigned resSZ = strlen(charStr) + sz + 1;
	char * res = new char[resSZ];
	strcpy_s(res, sz + 1, str);
	strcat(res, charStr);
	res[resSZ - 1] = '\0';
	String tmp(res);
	delete[] res;
	return tmp;
}
String& String::operator+=(const String& rhs)
{
	(*this) = (*this) + rhs.str;
	return *this;
}

unsigned String::getSZ() const
{
	return sz;
}

String::operator char*()
{
	return str;
}


std::istream & operator>>(std::istream& in, String& obj)
{
	char input[100];
	in >> input;
	obj = input;
	return in;
}
std::ostream & operator<<(std::ostream& out, const String& obj)
{
	out << obj.str;
	return out;
}


String toString(char * str)
{
	istringstream instr(str);
	String output;
	instr >> output;
	return output;
}

String toString(unsigned num)
{
	stringstream strStream;
	strStream << num;
	String res;
	strStream >> res;
	return res;
}

String toString(const char * str)
{
	istringstream instr(str);
	String output;
	instr >> output;
	return output;
}