#include <iostream>
#include "State.h"
#include <string>

State::State() : name(), isInitial(false), isFinal(false) {}

State::State(const String& str, bool isInitial, bool isFinal)
	: name(str), isInitial(isInitial), isFinal(isFinal) {}

State::State(const State& rhs)
{
	name = rhs.name;
	isInitial = rhs.isInitial;
	isFinal = rhs.isFinal;
}
State & State::operator=(const State& rhs) {
	if (this != &rhs)
	{
		name = rhs.name;
		isInitial = rhs.isInitial;
		isFinal = rhs.isFinal;
	}
	return *this;
}

State::~State() {}

void State::setStateName(const String& str)
{
	name = str;
}
void State::makeInitial()
{
	isInitial = true;
}

void State::makeFinal()
{
	if (!isFinal)
	{
		isFinal = true;
	}
}

void State::unfinalise()
{
	isFinal = false;
}


String State::getName() const
{
	return name;
}

bool State::isInitialState() const
{
	return isInitial;
}
bool State::isFinalState() const
{
	return isFinal;
}

bool State::operator==(const State & rhs)
{
	if (name == rhs.name)
	{
		return true;
	}
	return false;
}

std::istream & operator>>(std::istream & in, State & state)
{
	in >> state.name;
	return in;
}

std::ostream & operator<<(std::ostream & out, State & state)
{
	out << state.name;
	if (state.isInitial)
	{
		out << "(I)";
	}
	if (state.isFinal)
	{
		out << "(F)";
	}
	out << " ";
	return out;
}


unsigned findStatePosInStates(State st, State* states, unsigned sz)
{
	for (size_t i = 0; i < sz; i++)
	{
		if (st == states[i])
		{
			return i;
		}
	}
	return -1;
}