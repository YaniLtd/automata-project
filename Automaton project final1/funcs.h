#pragma once
#include "String.h"
#include "State.h"
#include "TransTable.h"
#include "AbstractDFA.h"
#include "DFA.h"
#include <cassert>


template<typename T>
void validOperation(AbstractDFA<T> * lhs, AbstractDFA<T> * rhs) 
{
	if (!(lhs->getAlphabet() == rhs->getAlphabet()))
	{
		cerr << "Mismatch in alphabets: Cannot perform operation!!!\n";
		throw AutomatonException("Mismatch in alphabets");
	}
}




template<typename T>
void generateFirstColCross(AbstractDFA<T> * lhs, AbstractDFA<T> * rhs, TransTable<T> & completeTable)
{
	unsigned lhsStatesCnt = lhs->getStatesCnt();
	unsigned rhsStatesCnt = rhs->getStatesCnt();
	unsigned i = 0;
	for (size_t j = 0; j < lhsStatesCnt; j++)
	{
		for (size_t k = 0; k < rhsStatesCnt; k++)
		{
			State currState;
			currState.setStateName(String("(") + lhs->getStates()[j].getName() + "," + rhs->getStates()[k].getName() + ")");
			if (lhs->getStates()[j].isFinalState() && rhs->getStates()[k].isFinalState())
			{
				currState.makeFinal();
			}
			if (lhs->getStates()[j].isInitialState() && rhs->getStates()[k].isInitialState())
			{
				currState.makeInitial();
			}
			completeTable.setElement(currState, i, 0);
			i++;
		}
	}
}

template<typename T>
void generateOtherColsCross(AbstractDFA<T> * lhs, AbstractDFA<T> * rhs, TransTable<T> & completeTable, unsigned rows, unsigned cols)
{
	unsigned lhsR = lhs->getStatesCnt();
	unsigned rhsR = rhs->getStatesCnt();
	unsigned r;
	for (size_t c = 0; c < cols - 1; c++)
	{
		r = 0;
		for (size_t i = 0; i < lhsR; i++)
		{
			for (size_t j = 0; j < rhsR; j++)
			{
				State currState;
				currState.setStateName(String("(") + lhs->getTable().getStateName(i, c) + "," + rhs->getTable().getStateName(j, c) + ")");
				if (lhs->getTable().getState(i, c).isFinalState() && rhs->getTable().getState(j, c).isFinalState())
				{
					currState.makeFinal();
				}
				if (lhs->getTable().getState(i, c).isInitialState() && rhs->getTable().getState(j, c).isInitialState())
				{
					currState.makeInitial();
				}
				completeTable.setElement(currState, r, c + 1);
				r++;
			}
		}
	}

}


template<typename T>
void generateFirstColUnion(AbstractDFA<T> * lhs, AbstractDFA<T> * rhs, TransTable<T> & completeTable)
{
	unsigned lhsStatesCnt = lhs->getStatesCnt();
	unsigned rhsStatesCnt = rhs->getStatesCnt();
	unsigned i = 0;
	for (size_t j = 0; j < lhsStatesCnt; j++)
	{
		for (size_t k = 0; k < rhsStatesCnt; k++)
		{
			State currState;
			currState.setStateName(String("(") + lhs->getStates()[j].getName() + "," + rhs->getStates()[k].getName() + ")");
			if (lhs->getStates()[j].isFinalState() || rhs->getStates()[k].isFinalState())
			{
				currState.makeFinal();
			}
			if (lhs->getStates()[j].isInitialState() && rhs->getStates()[k].isInitialState())
			{
				currState.makeInitial();
			}
			completeTable.setElement(currState, i, 0);
			i++;
		}
	}
}

template<typename T>
void generateOtherColsUnion(AbstractDFA<T> * lhs, AbstractDFA<T> * rhs, TransTable<T> & completeTable, unsigned rows, unsigned cols)
{
	unsigned lhsR = lhs->getStatesCnt();
	unsigned rhsR = rhs->getStatesCnt();
	unsigned r;
	for (size_t c = 0; c < cols-1; c++)
	{
		r = 0;
		for (size_t i = 0; i < lhsR; i++)
		{
			for (size_t j = 0; j < rhsR; j++)
			{
				State currState;
				currState.setStateName(String("(") + lhs->getTable().getStateName(i,c) + "," + rhs->getTable().getStateName(j, c) + ")");
				if (lhs->getTable().getState(i,c).isFinalState() || rhs->getTable().getState(j,c).isFinalState())
				{
					currState.makeFinal();
				}
				if (lhs->getTable().getState(i, c).isInitialState() && rhs->getTable().getState(j, c).isInitialState())
				{
					currState.makeInitial();
				}
				completeTable.setElement(currState, r, c+1);
				r++;
			}
		}
	}
	
}

template<typename T>
unsigned findStateAtRow(const TransTable<T> & completeTable, unsigned rows, const State & state)
{
	for (size_t r = 0; r < rows; r++)
	{
		if (completeTable.getState(r,0) == state)
		{
			return r;
		}
	}
	return -1;
}

bool containsState(State * states, unsigned sz, const State & state);


template<typename T>
void generateStates(TransTable<T> & completeTable, unsigned rows, unsigned cols, bool * markedStates, State * states, unsigned statesCnt, unsigned & idx) 
{
	bool tru;
	unsigned rowPos;
	do
	{
		tru = false;
		for (size_t i = 0; i < idx; i++)
		{
			if (markedStates[i] == false)
			{
				tru = true;
				rowPos = findStateAtRow(completeTable, rows, states[i]);
				markedStates[i] = true;
				for (size_t j = 1; j < cols; j++)
				{
					State currState(completeTable.getState(rowPos, j));
					if (!containsState(states, idx, currState))
					{
						states[idx++] = currState;
					}
				}
				break;
			}
		}
	}while (tru);
	/*cout << "States: ";
	for (size_t i = 0; i < idx; i++)
	{
		std::cout << states[i] << " ";
	}*/
}

template<typename T>
TransTable<T> generateTable(TransTable<T> & completeTable, unsigned rows, unsigned cols, State * states, unsigned statesCnt, Alphabet<T> alphabet)
{
	TransTable<T> table;
	table.createTable(statesCnt, cols - 1);
	unsigned rowPos;
	for (size_t i = 0; i < statesCnt; i++)
	{
		rowPos = findStateAtRow(completeTable, rows, states[i]);
		for (size_t j = 0; j < cols-1; j++)
		{
			table.setElement(completeTable.getState(rowPos, j+1), i, j);
			if (completeTable.getState(rowPos, j + 1).isInitialState())
			{
				table.makeInitialState(i, j + 1);
			}
			if (completeTable.getState(rowPos, j + 1).isFinalState())
			{
				table.makeFinalState(i, j + 1);
			}
		}
	}
	//std::cout << table;
	//table.print(cout, states, alphabet);
	return table;
}

template<typename T>
AbstractDFA<T> * operator|(AbstractDFA<T> & lhs, AbstractDFA<T> & rhs)
{
	validOperation(&lhs, &rhs);
	AbstractDFA<T> * resultAutomaton = new DFA<T>;
	TransTable<T> completeTable;
	unsigned completeTRows = lhs.getStatesCnt() * rhs.getStatesCnt();
	unsigned completeTCols = lhs.getAlphabetSymbolsCnt() + 1;
	completeTable.createTable(completeTRows, completeTCols);
	generateFirstColUnion(&lhs, &rhs, completeTable);
	generateOtherColsUnion(&lhs, &rhs, completeTable, completeTRows, completeTCols);

	unsigned statesCnt = completeTRows;
	bool * markedStates = new bool[statesCnt];
	for (size_t i = 0; i < statesCnt; i++)
	{
		markedStates[i] = false;
	}
	State * states = new State[statesCnt];
	states[0] = completeTable.getInitialState();
	unsigned idx = 1;
	generateStates(completeTable, completeTRows, completeTCols, markedStates, states, statesCnt, idx);
	statesCnt = idx;
	bool crash = true;
	for (size_t i = 0; i < statesCnt; i++)
	{
		if (states[i].isFinalState())
		{
			crash = false;
		}
	}
	assert(!crash);//no final states => invalid operation


	resultAutomaton->setStates(states, statesCnt);
	resultAutomaton->setAlphabet(lhs.getAlphabet(), lhs.getAlphabetSymbolsCnt());
	resultAutomaton->setTable(generateTable(completeTable, completeTRows, completeTCols, states, statesCnt, lhs.getAlphabet()), 0, 0);


	delete[] markedStates;
	delete[] states;
	return resultAutomaton;
}





template<typename T>
AbstractDFA<T> * operator&(AbstractDFA<T> & lhs, AbstractDFA<T> & rhs)
{
	validOperation(&lhs, &rhs);
	AbstractDFA<T> * resultAutomaton = new DFA<T>;
	TransTable<T> completeTable;
	unsigned completeTRows = lhs.getStatesCnt() * rhs.getStatesCnt();
	unsigned completeTCols = lhs.getAlphabetSymbolsCnt() + 1;
	completeTable.createTable(completeTRows, completeTCols);
	generateFirstColCross(&lhs, &rhs, completeTable);
	generateOtherColsCross(&lhs, &rhs, completeTable, completeTRows, completeTCols);

	unsigned statesCnt = completeTRows;
	bool * markedStates = new bool[statesCnt];
	for (size_t i = 0; i < statesCnt; i++)
	{
		markedStates[i] = false;
	}
	State * states = new State[statesCnt];
	states[0] = completeTable.getInitialState();
	unsigned idx = 1;
	generateStates(completeTable, completeTRows, completeTCols, markedStates, states, statesCnt, idx);
	statesCnt = idx;

	bool crash = true;
	for (size_t i = 0; i < statesCnt; i++)
	{
		if (states[i].isFinalState())
		{
			crash = false;
		}
	}
	assert(!crash);//no final states => invalid operation


	resultAutomaton->setStates(states, statesCnt);
	resultAutomaton->setAlphabet(lhs.getAlphabet(), lhs.getAlphabetSymbolsCnt());
	resultAutomaton->setTable(generateTable(completeTable, completeTRows, completeTCols, states, statesCnt, lhs.getAlphabet()), 0, 0);


	delete[] markedStates;
	delete[] states;
	return resultAutomaton;
}



template<typename T>
AbstractDFA<T> * operator!(const DFA<T> & automaton)
{
	DFA<T> * complement = new DFA<T>();
	*complement = automaton;
	complement->reverseFinalStates();

	return complement;
}