#pragma once
#include <iostream>

using namespace std;

class String
{
public:
	String();
	String(char *);
	String(const String&);
	~String();
	String & operator=(char *);
	String operator+(char *);
	String& operator+=(char *);
	bool operator==(char*);

	bool operator==(const String&);
	String & operator=(const String&);
	String operator+(const String&);
	String& operator+=(const String&);

	unsigned getSZ() const;

	operator char*();
	friend istream & operator>>(istream&, String&);
	friend ostream & operator<<(ostream&, const String&);
private:
	char * str; 	
	unsigned sz;
};

String toString(char * str);

String toString(unsigned num);

String toString(const char * str);