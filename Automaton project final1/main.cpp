#include <iostream>
#include <fstream>
#include "DFA.h"
#include "ConsoleMode.h"
#include "FileMode.h"
#include "TransTable.h"
#include "Operations.h"
#include "funcs.h"
#include "svgDisplay.h"


using namespace std;

int main()
{
	char answer;
	unsigned cnt = 1;
	do
	{
		String fileName = "Automaton";
		AbstractDFA<int> * atomantaInt[5];
		AbstractDFA<char> * atomantaChar[5];
		unsigned menu;
		unsigned idxI = 0;
		unsigned idxCh = 0;
		DFA<int> * rev = nullptr;
		DFA<char> * revCh = nullptr;
		for (size_t i = 0; i < 2; i++)
		{
			cout << "Choose mode (1 - for file input) or (2 - for console input): ";
			cin >> menu;
			switch (menu)
			{
			case 1:
				fileInput(atomantaInt, atomantaChar, idxI, idxCh);
				break;
			case 2:
				consoleInput(atomantaInt, atomantaChar, idxI, idxCh);
				break;
			default:
				cerr << "Invalid option!!!\n";
				break;
			}

		}
		fileName += toString(cnt);
		ofstream fout(fileName + ".txt");
		char op;
		cout << "choose an operation: |, &, ! ";
		cin >> op;
		if (idxCh>1)
		{
			switch (op)
			{
			case '|':
				atomantaChar[2] = (*atomantaChar[0]) | (*atomantaChar[1]);
				printSvg((*atomantaChar[2]), fileName);
				fout << (atomantaChar[2]);
				break;

			case '&':
				atomantaChar[2] = (*atomantaChar[0]) & (*atomantaChar[1]);
				printSvg((*atomantaChar[2]), fileName);
				fout << (atomantaChar[2]);
				break;

			case '!':
				cout << "get the complement of automaton 1 or 2: ";
				cin >> op;
				switch (op)
				{
				case '1':
					revCh = (DFA<char> *)atomantaChar[0];
					atomantaChar[1] = !(*revCh);
					printSvg((*atomantaChar[1]), fileName);
					fout << (atomantaChar[1]);
					break;

				case '2':
					revCh = (DFA<char> *)atomantaChar[1];
					atomantaChar[0] = !(*revCh);
					printSvg((*atomantaChar[0]), fileName);
					fout << (atomantaChar[0]);
					break;
				}
			}
		}
		else
		{
			switch (op)
			{
			case '|':
				atomantaInt[2] = (*atomantaInt[0]) | (*atomantaInt[1]);
				printSvg((*atomantaInt[2]), fileName);
				fout << (atomantaInt[2]);
				break;

			case '&':
				atomantaInt[2] = (*atomantaInt[0]) & (*atomantaInt[1]);
				fout << (atomantaInt[2]);
				printSvg((*atomantaInt[2]), fileName);
				break;

			case '!':
				cout << "get the complement of automaton 1 or 2: ";
				cin >> op;
				switch (op)
				{
				case '1':
					rev = (DFA<int> *)atomantaInt[0];
					atomantaInt[1] = !(*rev);
					printSvg((*atomantaInt[1]), fileName);
					fout << (atomantaInt[1]);
					break;

				case '2':
					rev = (DFA<int> *)atomantaInt[1];
					atomantaInt[0] = !(*rev);
					printSvg((*atomantaInt[0]), fileName);
					fout << (atomantaInt[0]);
					break;
				}
			}
		}
		
		cout << "Do you want to read more? y/n: ";
		cin >> answer;
		if (answer!='y')
		{
			break;
		}
		cnt++;
	} while (true);
	return 0;
}