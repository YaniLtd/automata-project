#pragma once
#include <iostream>
#include "Alphabet.h"
#include "String.h"
#include "State.h"
#include "TransTable.h"
#include "AutomatonException.h"
#include "AutomatonStateException.h"

template<typename T>
class AbstractDFA
{
public:
	AbstractDFA();
	AbstractDFA(State *, const TransTable<T>&, unsigned);
	AbstractDFA(const AbstractDFA<T>&);
	virtual ~AbstractDFA();
	AbstractDFA<T>& operator=(const AbstractDFA<T>&);


	void setStates(State *, unsigned);
	void setTable(const TransTable<T>, unsigned, unsigned);
	virtual void setAlphabet(const Alphabet<T> &, unsigned) = 0;


	void setStatesCnt(unsigned);
	void setStateNameAt(const String&, unsigned);
	virtual void setAlphabetSymbolsCnt(unsigned) = 0;
	virtual void setLetterAt(T, unsigned) = 0;
	void createTable(unsigned, unsigned);
	void setTableElement(const State&, unsigned, unsigned);
	void printTable(std::ostream &, State*, const Alphabet<T>&);


	 

	bool containsState(const String&, unsigned);
	bool containsState(const State&);
	bool hasInitial() const;
	int containsStateAt(const String &);


	void makeInitialState(unsigned);
	void makeFinalState(unsigned);
	void reverseFinalStates();
	virtual T getLetter(unsigned) const = 0;

	State * getStates() const;
	unsigned getStatesCnt() const;
	virtual unsigned getAlphabetSymbolsCnt() const = 0;
	virtual Alphabet<T> getAlphabet() const = 0;
	TransTable<T> getTable() const;
	bool isValid() const;


	void setInitialAndFinalStates() {
		unsigned rows = transTable.getRowsCnt();
		unsigned cols = transTable.getColsCnt();
		for (size_t i = 0; i < rows; i++)
		{
			for (size_t j = 0; j < cols; j++)
			{
				State currState(transTable.getState(i, j));
				for (size_t k = 0; k < statesCnt; k++)
				{
					if (currState == states[k])
					{
						if (states[k].isInitialState())
						{
							transTable.makeInitialState(i,j);
						}
						if (states[k].isFinalState())
						{
							transTable.makeFinalState(i, j);
						}
						break;
					}
				}
			}
		}
	}
protected:
	State * states; //Q
	TransTable<T> transTable;
	unsigned statesCnt;
};




template<typename T>
AbstractDFA<T>::AbstractDFA()
	: states(nullptr), transTable(), statesCnt(0) {}

template<typename T>
AbstractDFA<T>::AbstractDFA(State * s, const TransTable<T>& t, unsigned sCnt)
	: transTable(t), statesCnt(sCnt)
{
	for (size_t i = 0; i < sCnt; i++)
	{
		states[i] = s[i];
	}
}

template<typename T>
AbstractDFA<T>::AbstractDFA(const AbstractDFA<T>& rhs)
	: transTable(rhs.transTable), statesCnt(rhs.statesCnt)
{
	for (size_t i = 0; i < statesCnt; i++)
	{
		states[i] = rhs.states[i];
	}

}

template<typename T>
AbstractDFA<T>::~AbstractDFA()
{
	if (states != nullptr)
	{
		delete[] states;
	}
}

template<typename T>
AbstractDFA<T>& AbstractDFA<T>::operator=(const AbstractDFA<T>& rhs)
{
	if (this != &rhs)
	{
		transTable = rhs.transTable;
		statesCnt = rhs.statesCnt;
		if (states != nullptr)
		{
			delete[] states;
		}
		states = new State[statesCnt];
		for (size_t i = 0; i < statesCnt; i++)
		{
			states[i] = rhs.getStates()[i];
		}
	}
	return *this;
}

template<typename T>
void AbstractDFA<T>::setStates(State * states, unsigned sz)
{
	statesCnt = sz;
	if (this->states != nullptr)
	{
		delete this->states;
	}
	this->states = new State[sz];
	for (size_t i = 0; i < sz; i++)
	{
		this->states[i] = states[i];
	}
}

template<typename T>
void AbstractDFA<T>::setTable(const TransTable<T> t, unsigned rows, unsigned cols)
{
	transTable = t;
}

template<typename T>
void AbstractDFA<T>::setStatesCnt(unsigned cnt)
{
	statesCnt = cnt;
	if (states != nullptr)
	{
		delete[] states;
	}
	states = new State[statesCnt];
}

template<typename T>
void AbstractDFA<T>::setStateNameAt(const String& name, unsigned idx)
{
	states[idx].setStateName(name);
}

template<typename T>
void AbstractDFA<T>::createTable(unsigned r, unsigned c)
{
	transTable.createTable(r, c);
}

template<typename T>
void AbstractDFA<T>::setTableElement(const State & state, unsigned i, unsigned j)
{
	transTable.setElement(state, i, j);
}

template<typename T>
void AbstractDFA<T>::printTable(std::ostream & out, State* s, const Alphabet<T>& abc)
{
	transTable.print(out, s, abc);
}



template<typename T>
bool AbstractDFA<T>::containsState(const String& name, unsigned bound)
{
	for (size_t i = 0; i < bound; i++)
	{
		if (states[i].getName() == name)
		{
			return true;
		}
	}
	return false;
}

template<typename T>
bool AbstractDFA<T>::containsState(const State & state)
{
	for (size_t i = 0; i < statesCnt; i++)
	{
		if (states[i] == state)
		{
			return true;
		}
	}
	return false;
}

template<typename T>
bool AbstractDFA<T>::hasInitial() const
{
	for (size_t i = 0; i < statesCnt; i++)
	{
		if (states[i].isInitialState())
		{
			return true;
		}
	}
	return false;
}

template<typename T>
int AbstractDFA<T>::containsStateAt(const String& name)
{
	for (size_t i = 0; i < statesCnt; i++)
	{
		if (states[i].getName() == name)
		{
			return i;
		}
	}
	return -1;
}

template<typename T>
State * AbstractDFA<T>::getStates() const
{
	return states;
}

template<typename T>
void AbstractDFA<T>::makeInitialState(unsigned idx)
{
	if (idx<statesCnt)
	{
		states[idx].makeInitial();
	}
}

template<typename T>
void AbstractDFA<T>::makeFinalState(unsigned idx)
{
	if (idx<statesCnt)
	{
		states[idx].makeFinal();
	}
}

template<typename T>
void AbstractDFA<T>::reverseFinalStates()
{
	for (size_t i = 0; i < statesCnt; i++) 
	{
		if (states[i].isFinalState()) 
		{
			states[i].unfinalise();
		}
		else 
		{
			states[i].makeFinal();
		}
	}

	transTable.reverseFinalStates();
}


template<typename T>
unsigned AbstractDFA<T>::getStatesCnt() const
{
	return statesCnt;
}

template<typename T>
TransTable<T> AbstractDFA<T>::getTable() const
{
	return transTable;
}

template<typename T>
bool AbstractDFA<T>::isValid() const
{
	unsigned cnt = 0;
	for (size_t i = 0; i < statesCnt; i++)
	{
		if (states[i].isInitialState())
		{
			cnt++;
		}
	}
	if (cnt > 1)
	{
		throw AutomatonException();
	}
	for (size_t i = 0; i < statesCnt; i++)
	{
		if (states[i].isFinalState())
		{
			return true;
		}
	}
	throw AutomatonException();
	return false;
}

