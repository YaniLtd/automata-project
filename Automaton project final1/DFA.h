#pragma once
#include "Alphabet.h"
#include "State.h"
#include "AbstractDFA.h"


template<typename T>
class DFA : public AbstractDFA<T>
{
public:
	DFA();
	DFA(State *, const TransTable<T> &, unsigned, const Alphabet<T>&, unsigned);
	DFA(const DFA<T>&);
	~DFA();
	DFA<T>& operator=(const DFA<T>&);


	void setAlphabet(const Alphabet<T> &, unsigned);
	virtual void setAlphabetSymbolsCnt(unsigned);
	virtual void setLetterAt(T, unsigned);

	virtual T getLetter(unsigned idx) const;
	virtual unsigned getAlphabetSymbolsCnt() const;
	virtual Alphabet<T> getAlphabet() const;

	

private:
	bool containsSymbol(char, unsigned idx) const;
	Alphabet<T> alphabet;
	unsigned lettersCnt;
};

template<typename T>
DFA<T>::DFA() : alphabet(), lettersCnt(0) {}

template<typename T>
DFA<T>::DFA(State * states, const TransTable<T> & transTable, unsigned statesCnt, const Alphabet<T>& alphabet, unsigned symbolsCnt)
	: AbstractDFA<T>(states, transTable, statesCnt), alphabet(alphabet), lettersCnt(symbolsCnt) {}

template<typename T>
DFA<T>::DFA(const DFA<T>& obj)
	: AbstractDFA<T>(obj), alphabet(obj.alphabet), lettersCnt(obj.lettersCnt) {}

template<typename T>
DFA<T>::~DFA() {}

template<typename T>
DFA<T>& DFA<T>::operator=(const DFA<T>& rhs)
{
	if (this != &rhs)
	{
		AbstractDFA<T>::operator = (rhs);
		alphabet = rhs.alphabet;
		lettersCnt = rhs.lettersCnt;
	}
	return *this;
}





template<typename T>
bool DFA<T>::containsSymbol(char ch, unsigned idx) const
{
	for (size_t i = 0; i < idx; i++)
	{
		if (alphabet[i] == ch)
		{
			return true;
		}
	}
	return false;
}



template<typename T>
void DFA<T>::setAlphabet(const Alphabet<T> & alph, unsigned sz)
{
	lettersCnt = sz;
	alphabet = alph;
}

template<typename T>
void DFA<T>::setAlphabetSymbolsCnt(unsigned n)
{
	lettersCnt = n;
	alphabet.setLettersCnt(n);
}

template<typename T>
void DFA<T>::setLetterAt(T l, unsigned idx)
{
	alphabet[idx] = l;
}

template<typename T>
T DFA<T>::getLetter(unsigned idx) const
{
	return alphabet[idx];
}

template<typename T>
unsigned DFA<T>::getAlphabetSymbolsCnt() const
{
	return lettersCnt;
}

template<typename T>
Alphabet<T> DFA<T>::getAlphabet() const
{
	return alphabet;
}

//template<typename T>
//inline BaseDFA DFA<T>::operator&(const BaseDFA&)
//{
//	return BaseDFA();
//}
//
//template<typename T>
//BaseDFA DFA<T>::operator|(AbstractDFA<T> * rhs)
//{
//	
//	/// set alphabet
//	AbstractDFA<T>* unitedDFA = new DFA<T>;
//	unitedDFA->setAlphabetSymbolsCnt(alphSZ);
//	for (size_t i = 0; i < alphSZ; i++)
//	{
//		unitedDFA->setLetterAt(rhs->getAlphabet()[i], i);
//	}
//
//
//	/// set states
//	
//	//all automatons have at least 1 initial and 1 final
//	for (size_t i = 0; i < statesResultCnt; i++)
//	{
//		currStateName = "(";
//		currStateName += AbstractDFA<T>(*this).getStates()[i].getName();//static upcast
//		currStateName += ", ";
//		currStateName += rhs->getStates()[i].getName();
//		currStateName += ")";
//		if (true)
//		{
//			cartesianProduct[i] = currStateName;
//		}
//	}
//
//	unitedDFA->setStatesCnt(statesResultCnt);
//	String stateName;
//	size_t k = 0;
//	for (size_t i = 0; i < statesLhsCnt; i++)
//	{
//		for (size_t j = 0; j < statesRhsCnt; j++)
//		{
//
//			stateName += "(";
//			stateName += this->getStates()[i].getName();
//			stateName += ", ";
//			stateName += rhs->getStates()[j].getName();
//			stateName += ")";
//			unitedDFA->setStateNameAt(stateName, i);
//			if (rhs->getStates()[j].isInitialState() && this->getStates()[i].isInitialState())
//			{
//				unitedDFA->getStates()[k].makeInitial();
//			}
//			if (rhs->getStates()[j].isFinalState() || this->getStates()[i].isFinalState())
//			{
//				unitedDFA->getStates()[k].makeInitial();
//			}
//			k++;
//		}
//	}
//
//
//	delete[] cartesianProduct;
//
//	/// set table
//
//
//	return *unitedDFA;
//}
//
//
//
//template<typename T>
//BaseDFA DFA<T>::operator!()
//{
//	DFA<T> complement(*this);
//	for (size_t i = 0; i < complement.statesCnt; i++)
//	{
//		if (complement.states[i].isFinalState())
//		{
//			complement.states[i].unfinalise();
//		}
//		else
//		{
//			complement.states[i].makeFinal();
//		}
//	}
//	return complement;
//}
//
//
//
//
//
//
//
