#include <iostream>
#include "ConsoleMode.h"
#include "FileMode.h"
#include "DFA.h"
#include "Operations.h"
#include "String.h"
bool isSeparator(char ch)
{
	switch (ch)
	{
	case '\n':
	case ' ':
	case '\t':
		return true;
	default:
		return false;
	}
}
void fileInput(AbstractDFA<int> *(&intAutomata)[5], AbstractDFA<char> * (&charAutomata)[5], unsigned & idxI, unsigned & idxCh) {
	AbstractDFA<int> * automatonInt = nullptr;
	AbstractDFA<char> * automatonChar = nullptr;
	char ch;
	
	
	char fileName[200];
	cout << "Enter name of file: ";

	do//clearing many separators
	{
		std::cin.get(ch);
	} while (isSeparator(ch));
	std::cin.putback(ch);

	cin.getline(fileName, 199);

	ifstream fileIn(fileName);
	if (!fileIn)
	{
		std::cerr << "Couldn't open the file";
		throw;
	}

	unsigned menu;
	fileIn >> menu;
	switch (menu)
	{
	case 1:
		automatonInt = new DFA<int>;
		fileIn >> automatonInt;
		break;
	case 2:
		automatonChar = new DFA<char>;
		fileIn >> automatonChar;
		break;
	default:
		std::cerr << "Invalid option!!!\n";
		throw;
		break;
	}

	fileIn.close();
	String outputFileName("Out");
	outputFileName += fileName;
	ofstream fileOut(outputFileName);
	if (!fileOut)
	{
		cerr << "Could not generate or open an output file\n"
			<< "Here is the generated automaton:\n";
		if (automatonInt != nullptr)
		{
			std::cout << automatonInt;
			intAutomata[idxI] = automatonInt;
			idxI++;
		}

		if (automatonChar != nullptr)
		{
			std::cout << automatonChar;
			charAutomata[idxCh] = automatonChar;
			idxCh++;
		}
	}
	else
	{
		if (automatonInt != nullptr)
		{
			fileOut << automatonInt;
			intAutomata[idxI] = automatonInt;
			idxI++;
		}

		if (automatonChar != nullptr)
		{
			fileOut << automatonChar;
			charAutomata[idxCh] = automatonChar;
			idxCh++;
		}
	}
	
}


void consoleInput(AbstractDFA<int> *(&intAutomata)[5], AbstractDFA<char> * (&charAutomata)[5], unsigned & idxI, unsigned & idxCh) {
	AbstractDFA<int> * automatonInt = nullptr;
	AbstractDFA<char> * automatonChar = nullptr;
	unsigned menu;

	std::cout << "Choose type of alphabet (1 - for int) or (2 - for char): ";
	std::cin >> menu;
	switch (menu)
	{
	case 1:
		automatonInt = new DFA<int>;
		std::cin >> automatonInt;
		break;
	case 2:
		automatonChar = new DFA<char>;
		std::cin >> automatonChar;
		break;
	default:
		std::cerr << "Invalid option!!!\n";
		throw;
		break;
	}

	if (automatonInt != nullptr)
	{
		std::cout << automatonInt;
		intAutomata[idxI] = automatonInt;
		idxI++;
	}

	if (automatonChar != nullptr)
	{
		std::cout << automatonChar;
		charAutomata[idxCh] = automatonChar;
		idxCh++;
	}
}