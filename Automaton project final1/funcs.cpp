#include "funcs.h"
#include "String.h"
#include "State.h"
#include "DFA.h"


bool containsState(State* states, const String& name, unsigned bound)
{
	for (size_t i = 0; i < bound; i++)
	{
		if (states[i].getName() == name)
		{
			return true;
		}
	}
	return false;
}
bool containsName(String* states, const String& name, unsigned bound)
{
	for (size_t i = 0; i < bound; i++)
	{
		if (states[i] == name)
		{
			return true;
		}
	}
	return false;
}

bool containsState(State * states, unsigned sz, const State & state)
{
	
		for (size_t i = 0; i < sz; i++)
		{
			if (states[i] == state)
			{
				return true;
			}
		}
		return false;
	
}
