#pragma once
#include "String.h"
class State
{
public:
	State();
	State(const String&, bool = false, bool = false);
	State(const State&);
	State & operator=(const State&);
	~State();
	void makeInitial();
	void makeFinal();
	void unfinalise();
	void setStateName(const String&);
	String getName() const;
	bool isInitialState() const;
	bool isFinalState() const;
	bool operator==(const State&);

	friend std::istream & operator>>(std::istream &, State&);
	friend std::ostream & operator<<(std::ostream &, State&);
private:
	String name;
	bool isFinal;
	bool isInitial;
};


unsigned findStatePosInStates(State, State*, unsigned);
