#include <iostream>
#include <fstream>
#include <cmath>
#include "DFA.h"
#include "String.h"
#include "funcs.h"
#include "svgDisplay.h"




void printCirlce(std::ostream & outSvg, const Circle& circle)
{
	outSvg << "<circle cx = "
		<< "\"" << circle.cx << "\"" << " cy = " << "\"" << circle.cy << "\"" << " r = "
		<< "\"" << circle.r << "\"" << " stroke = " << "black" << " stroke-width = " << "\"" << 4 << "\""
		<< " fill = " << "white />\n";
}

void printInnerStroke(std::ostream & outSvg, const Circle& circle)
{
	outSvg << "<circle cx = "
		<< "\"" << circle.cx << "\"" << " cy = " << "\"" << circle.cy << "\"" << " r = "
		<< "\"" << circle.r << "\"" << " stroke = " << "black" << " stroke-width = " << "\"" << 4 << "\""
		<< " fill = " << "none />\n";
}

void drawInitialArrow(std::ostream & outSvg, const Circle & circleA)
{//drawing initial state
	Point A;
	Point B;
	A.x = 0;
	A.y = circleA.cy;

	B.x = circleA.cx - circleA.r;
	B.y = circleA.cy;
	//<line x1="0" y1="0" x2="200" y2="200" style="stroke:rgb(255,0,0);stroke-width:2" />

	outSvg << "\n<line  x1 = " << A.x << " y1=" << A.y
		<< " x2 = " << B.x << " y2 = " << B.y
		<< " style= stroke:black stroke-width:5/>";

	float helper = circleA.r / 4;
	outSvg << "<polygon points = \""
		<< B.x << " , " << B.y << " , "
		<< B.x - helper << " , " << B.y + helper
		<< " , " << B.x - helper << " , " << B.y - helper << "\"" << "/>";
}



void setCircleName(std::ofstream & outSvg, const String & name, float cx, float cy)
{
	outSvg << "<text x = " << cx - 20 << " y = " << cy + 3 << ">" << name << "</text>";
}







