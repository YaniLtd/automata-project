#pragma once
#include "DFA.h"

//1
//unsigned readType()
//{
//	/*
//	1. ��������� �� ��� �� �������� � �������� (1 - ���� �����, 2 � �������� ���): 1
//	��� ����� �� �������� 1, ��� ��������� �� ���
//	���������� ������� �� �������� ����� �� ��� DeterminateciniteAutomatonInt.
//	��� ����� �� �������� 2, �� ��
//	������������ ���������� � ����� �� ��� DeterminateciniteAutomatonChar
//	*/
//	unsigned menu;
//	bool f = false;
//	do
//	{
//		std::cout << "Choose the type for your alphabet:\n1 - for whole numbers or 2 - for symbols\n";
//		std::cin >> menu;
//		switch (menu)
//		{
//		case 1:
//			f = false;
//			break;
//		case 2:
//			f = false;
//			break;
//		default:
//			f = true;
//		}
//	} while (f);
//	return menu;
//}

//2. ���� �������� ���� �����������: 2
template<typename T>
void readStatesCnt(AbstractDFA<T> * p)
{
	bool f;
	unsigned cnt;
	do
	{
		f = false;
		std::cout << "Enter number of states (between 1 - 50)\n";
		std::cin >> cnt;
		if (0 == cnt || cnt > 50)
		{
			f = true;
		}
	} while (f);
	p->setStatesCnt(cnt);
}


/*
3. ���� �������� ��������� �����[1]: q0
4. ���� �������� ��������� �����[2] : q1
? ������� e ����� �� �����������, ������� �� ��� 2, ������� �� ���� ��������� �
���������� �� �� ������� ������������.? /
*/
template<typename T>
void readStates(AbstractDFA<T> * p)
{
	unsigned i = 0;
	unsigned sz = p->getStatesCnt();
	String stateName;
	while (i<sz)
	{
		std::cout << "Please enter state number[" << i + 1 << "]: ";
		cin >> stateName;
		if (!p->containsState(stateName, i))
		{
			p->setStateNameAt(stateName, i);
			i++;
		}
		else
		{
			std::cerr << "State name already exists!!!\n";
		}
	}

}

//5. ���� �������� ���� ������� � �������� �� ��������: 2
template<typename T>
void readAlphabetSymbolsCnt(AbstractDFA<T> * p)
{
	bool f;
	unsigned cnt;
	do
	{
		f = false;
		std::cout << "Enter number of symbols for the alphabet (between 1 - 10)\n";
		std::cin >> cnt;
		if (0 == cnt || cnt > 10)
		{
			std::cerr << "Number of symbols entered was not in range [1-10]!!!!!\n";
			f = true;
		}
	} while (f);
	p->setAlphabetSymbolsCnt(cnt);

}


/*
6. ���� �������� ������ ����� [1]: 0
7. ���� �������� ������ ����� [2]: 1
? ������� � ����� �� ��������� � ��������, ������� �� ��� 5, ������� �� ����
������� �� �������� � ���������� �� �� ������� ������������. ?/
*/
template<typename T>
void readAlphabet(AbstractDFA<T> * p)
{
	unsigned i = 0;
	T letter;
	unsigned sz = p->getAlphabetSymbolsCnt();
	while (i < sz)
	{
		std::cout << "Enter letter [" << i + 1 << "]: ";
		std::cin >> letter;
		if (p->getAlphabet().containsLetter(letter, i))
		{
			std::cerr << "Letter already exists!!!\n";
		}
		else
		{
			p->setLetterAt(letter, i);
			i++;
		}
	}
}


/*
? �� ���� �� ���� ��������� N � ���� �������� �� �������� , �� �� ������� ��
����������� �� �� ������ Transition table � N ?M ��������. � ����� ������ �����
2 ��������� � 2 �������� �� ��������, ������������ 4 �������� � ��������� �
���������, ������ �� ����� ��������� � ������ �� ������ ��� ��� ��������� ���� ?/

[8-11]
*/
template<typename T>
void readTransTable(AbstractDFA<T> * p)
{
	unsigned rows = p->getStatesCnt();
	unsigned cols = p->getAlphabetSymbolsCnt();
	p->createTable(rows, cols);
	size_t j;
	State input;
	for (size_t i = 0; i < rows; i++)
	{
		j = 0;
		while (j < cols)
		{
			std::cout << "Enter (" << p->getStates()[i].getName() << ", " << p->getLetter(j) << ")->";
			std::cin >> input;
			if (p->containsState(input))
			{
				p->setTableElement(input, i, j);
				j++;
			}
			else
			{
				std::cerr << "State does not exist!!!\n";
			}
		}
	}
	//p->printTable(std::cout, p->getStates(), p->getAlphabet());

}

//12. ���� �������� ������� ���������: q0
template<typename T>
void readInitialState(AbstractDFA<T> * p)
{
	if (p->hasInitial())
	{
		std::cerr << "The automaton already has an initial state!!!\n";
		return;
	}
	char stateName[10];
	int idx;
	bool f;
	do
	{
		f = false;
		std::cout << "Enter initial state: ";
		std::cin >> stateName;
		idx = p->containsStateAt(stateName);
		if (idx == -1)
		{
			std::cerr << "Wrong state name!!!\n";
			f = true;
		}
		else
		{
			p->makeInitialState(idx);
		}
	} while (f);

}

//13. ���� �������� ���� ������ ���������: 1
template<typename T>
void readFinalStates(AbstractDFA<T> * p)
{
	unsigned fStatesCnt;
	bool f;
	unsigned statesCnt = p->getStatesCnt();
	do
	{
		f = false;
		std::cout << "Enter number of final states: ";
		std::cin >> fStatesCnt;
		if (fStatesCnt > statesCnt || fStatesCnt == 0)
		{
			f = true;
			std::cerr << "The number of final states must be between [" << 1 << "-" << statesCnt << "]" << "!!!\n";
		}
	} while (f);


	char stateName[10];
	int idx;
	unsigned i = 0;
	while (i < fStatesCnt)
	{
		f = false;
		std::cout << "Enter final state: ";
		std::cin >> stateName;
		idx = p->containsStateAt(stateName);
		if (idx == -1)
		{
			std::cerr << "Wrong state name!!!\n";
			f = true;
		}
		else if (p->getStates()[idx].isFinalState())
		{
			std::cerr << "This state is already final!!!\n";
			f = true;
		}
		else
		{
			p->makeFinalState(idx);
			i++;
		}
	}


}
template<typename T>
std::istream & operator>>(std::istream& in, AbstractDFA<T> * p)
{
	readStatesCnt(p);
	readStates(p);
	readAlphabetSymbolsCnt(p);
	readAlphabet(p);
	readTransTable(p);
	readInitialState(p);
	readFinalStates(p);
	p->setInitialAndFinalStates();
	p->isValid();
	return in;
}


void consoleInput(AbstractDFA<int> *(&intAutomata)[5], AbstractDFA<char> * (&charAutomata)[5], unsigned & idxI, unsigned & idxCh);