#include <iostream>
#include <fstream>
#include <cmath>
#include "DFA.h"
#include "String.h"
#include "funcs.h"

struct Point
{
	float x, y;
};

struct Circle
{
	float cx, cy, r;
};


template<typename T>
void drawCurveForward(std::ostream & outSvg, T letter, unsigned letterNum, float height, bool isMarked,
	const Circle& circleA, const Circle& circleB, int stateDif)
{

	Point A;
	Point B;
	Point Mid;
	Point txtPlace;

	if (stateDif == 1)
	{
		A.x = circleA.cx + circleA.r;
		A.y = circleA.cy;

		B.x = circleB.r;
		B.y = 0;

		txtPlace.x = (A.x) + circleA.r/ 2;
		txtPlace.y = A.y - circleA.r / 4;

		outSvg << "\n<path d = \"" << "m " << A.x << " " << A.y << " "
			<< "l " << B.x << " " << B.y << "\" "
			<< "stroke=black stroke-width=5 fill=none />";

		outSvg << "<text x = " << txtPlace.x - (14 / letterNum) << " y = " << txtPlace.y << ">" << letter << "</text>";

		B.x = circleB.cx - circleB.r;
		B.y = circleB.cy;

		float helper = circleB.r/3;
		outSvg << "<polygon points = \""
			<< B.x << " , " << B.y << " , "
			<< B.x - helper << " , " << B.y + helper
			<< " , " << B.x - helper << " , " << B.y - helper << "\"" << "/>";


	}
	else
	{
		if (isMarked)
		{
			stateDif++;
			A.x = circleA.cx + 17;
			A.y = circleA.cy - circleA.r + 6;

			B.x = circleB.cx - A.x + 17;
			B.y = -1;

			Mid.x = B.x / 1.2;
			Mid.y = (stateDif * height) / (height*0.01);
			Mid.y *= (-1);
			outSvg << "\n<path d = \"" << "m " << A.x << " " << A.y << " "
				<< "q " << Mid.x << " " << Mid.y << " "
				<< B.x << " " << B.y << "\" "
				<< "stroke=black stroke-width=5 fill=none />";
			txtPlace.x = Mid.x + A.x;
			txtPlace.y = (Mid.y/2) + A.y;

			outSvg << "<text x = " << txtPlace.x - (14 / letterNum) << " y = " << txtPlace.y << ">" << letter << "</text>";

			B.x = circleB.cx + circleB.r / 2 - 4;
			B.y = circleB.cy - circleB.r + 4;
			float helper = circleB.r/3;
			outSvg << "\n\n\n<polygon points = \" "
				<< B.x << "," << B.y << " "
				<< B.x - helper << "," << B.y - helper
				<< " " << B.x + helper << "," << B.y - helper << "\"/>";
		}
		else
		{
			stateDif++;
			A.x = circleA.cx + circleA.r/2;
			A.y = circleA.cy + 2*circleA.r/3 + 6;

			B.x = circleB.cx - A.x;
			B.y = 10;

			Mid.x = B.x / 2;
			Mid.y = (stateDif * height) / (height*0.01);
			outSvg << "\n<path d = \"" << "m " << A.x << " " << A.y << " "
				<< "q " << Mid.x << " " << Mid.y << " "
				<< B.x << " " << B.y << "\" "
				<< "stroke=black stroke-width=5 fill=none />";
			txtPlace.x = Mid.x + A.x;
			txtPlace.y = (Mid.y / 2) + A.y + A.y/35;
			//txtPlace.y = 2*(Mid.y)/3 + A.y;
			
			outSvg << "<text x = " << txtPlace.x - (14 / letterNum) << " y = " << txtPlace.y << ">" << letter << "</text>";

			////////////////////////
			float helper = circleB.r/3;
			B.x = circleB.cx - 5;
			B.y = circleB.cy + circleB.r - 5;
			outSvg << "<polygon points = \" "
				<< B.x << "," << B.y << " "
				<< B.x - helper << "," << B.y + helper
				<< " " << B.x + helper << "," << B.y + helper << "\"/>";
		}

	}
}


template<typename T>
void drawCurveBackwards(std::ostream & outSvg, T letter, unsigned letterNum, float height, bool isMarked,
	const Circle & circleA, const Circle & circleB, int stateDif)
{
	Point A;
	Point B;
	Point Mid;
	Point txtPlace;

	{
		A.x = circleA.cx;
		A.y = circleA.cy + circleA.r;

		B.x = (stateDif) * (-1 * (3 * circleA.r));
		B.y = 0;

		Mid.x = B.x * 0.75;
		Mid.y =  (stateDif * height) / (height*0.01);
		//Mid.y = (stateDif)* circleA.r;

		outSvg << "\n<path d = \"" << "m " << A.x << " " << A.y << " "
			<< "q " << Mid.x << " " << Mid.y << " "
			<< B.x << " " << B.y << "\" "
			<< "stroke=black stroke-width=5 fill=none />";


		txtPlace.x = Mid.x + A.x;
		txtPlace.y = (Mid.y / 2) + A.y + A.y / 35;
		//txtPlace.y = 2 * (Mid.y) / 3 + A.y;

		outSvg << "<text x = " << txtPlace.x  << " y = " << txtPlace.y << ">" << letter << "</text>";


		B.x = circleB.cx;
		B.y = circleB.cy + circleB.r;

		float helper = circleB.r/3;
		outSvg << "<polygon points = \" "
			<< B.x << "," << B.y << " "
			<< B.x - helper << "," << B.y + helper
			<< " " << B.x + helper << "," << B.y + helper << "\"/>";
	}
}

void printCirlce(std::ostream & outSvg, const Circle& circle);
void printInnerStroke(std::ostream & outSvg, const Circle& circle);

void drawInitialArrow(std::ostream & outSvg, const Circle& circleA);


template<typename T>
void printRelation(std::ostream & outSvg, T letter, unsigned letterNum, float height, unsigned division, bool isMarked,
	bool isAhead, const Circle& circleA, const Circle& circleB, int stateDif)
{
	Point A;
	Point B;
	Point Mid;
	Point txtPlace;


	if (stateDif == 0)//////////// loop
	{
		A.x = circleA.cx - 35;
		A.y = circleA.cy - 20;

		B.x = 35;
		B.y = -20;

		Mid.x = B.x / 2;
		Mid.y = -(height*0.1);

		outSvg << "\n<path d = \"" << "m " << A.x << " " << A.y << " "
			<< "q " << Mid.x << " " << Mid.y << " "
			<< B.x << " " << B.y << "\" "
			<< "stroke=black stroke-width=5 fill=none />";

		txtPlace.x = Mid.x + A.x;
		txtPlace.y = (-3*circleA.r)/2 + A.y;
		outSvg << "<text x = " << txtPlace.x - (14 / letterNum) << " y = " << txtPlace.y << ">" << letter << "</text>";

		B.x = circleB.cx;
		B.y = circleB.cy - circleB.r;
		float helper = circleB.r/3;
		outSvg << "<polygon points = \""
			<< B.x << " , " << B.y << " , " << B.x - helper << " , "
			<< B.y - helper << " , " << B.x + helper << " , "
			<< B.y - helper << "\"" << "/>";
	}
	else if (isAhead)
	{
		drawCurveForward(outSvg, letter, letterNum, height, isMarked,
			circleA, circleB, stateDif);
	}
	else
	{
		drawCurveBackwards(outSvg, letter, letterNum, height, isMarked,
			circleA, circleB, stateDif);
	}
}


void setCircleName(std::ofstream & outSvg, const String & name, float cx, float cy);

template<typename T>
void printSvg(const AbstractDFA<T> & automaton, String & svgFileName) {

	std::ofstream outSvg(svgFileName + ".html");
	outSvg << "<!DOCTYPE html>\n";
	outSvg << "<html>\n";
	outSvg << "<body>\n";

	float height = 1000, width = 1500;
	outSvg << "<svg width = " << height << " height = " << width << ">\n";
	unsigned statesCnt = automaton.getStatesCnt();
	Circle * circles = new Circle[statesCnt];
	bool * marked = new bool[statesCnt];
	for (size_t i = 0; i < statesCnt; i++)
	{
		marked[i] = false;
	}
	float cx = width / statesCnt / 2, cy = height / 2, r = 40;
	circles[0].cx = cx;
	circles[0].cy = cy;
	circles[0].r = r;
	drawInitialArrow(outSvg, circles[0]);
	for (size_t i = 0; i < statesCnt; i++)
	{
		circles[i].cx = cx + i * 3 * r;
		circles[i].cy = cy;
		circles[i].r = r;
		printCirlce(outSvg, circles[i]);
		if (automaton.getStates()[i].isFinalState())
		{
			automaton.getStates()[i].isFinalState();
			circles[i].r = r-6;
			printInnerStroke(outSvg, circles[i]);
			circles[i].r = r;
		}
		setCircleName(outSvg, automaton.getStates()[i].getName(), circles[i].cx, circles[i].cy);
	}
	unsigned relStPos;
	int dif;
	bool isAhead;
	for (size_t i = 0; i < statesCnt; i++)
	{
		for (size_t j = 0; j < automaton.getAlphabetSymbolsCnt(); j++)
		{
			relStPos = findStatePosInStates(automaton.getTable().getState(i, j), automaton.getStates(), statesCnt);
			dif = i - relStPos;
			isAhead = (i > relStPos) ? false : true;
			dif = abs(dif);
			
			printRelation(outSvg, automaton.getLetter(j), j+1, height, statesCnt, marked[i], isAhead, circles[i], circles[relStPos], dif);
			marked[i] = true;
		}
	}
	outSvg << "\n</svg>";
	outSvg << "</body>";
	outSvg << "</html>";
	outSvg.close();
}


